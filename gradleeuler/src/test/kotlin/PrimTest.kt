import util.primes
import java.lang.Math.pow

object PrimTest {

    @JvmStatic
    fun main(args: Array<String>) {
        println(primes().take(pow(350000.0,5.0).toInt()).last())//euler.main(args);
    }
}
