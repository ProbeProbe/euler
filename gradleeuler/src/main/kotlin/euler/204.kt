package euler

import util.modfac
import util.primes

fun main(args: Array<String>) {
    val min = 5
//    val max=100
    val max = 100_000_000
    var sum = 0L

//    val p = 7;
    var i = 0
    for (p in primes().takeWhile { it <= max }.filter { it >= min }) {
        i++;
        if (i % (max / 1000) == 0)
            println(p)

//        println(p)
        var sum2 = 0L
        var fac = modfac(p - 6, p);
        for (k in 5 downTo 1) {
            fac = (fac * (p - k)) % p;
            fac = Math.max(fac, 1)
            sum2 += fac
        }
//        println("$p -> ${sum2%p}")
        sum += sum2 % p;
    }
    println("Sum: $sum")
}
