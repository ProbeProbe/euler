package euler

import util.primes

/*
    P(n)=prod(k, p(n,k))
    c(u)=#!prim(u)=k
    p(n,k)=#piSeq[u0<=n /\ c(u)=k]
        =sum(u0 -> n, #piSeq(u0)[#!prim=k])
 */

fun main(args: Array<String>) {

    println(piSeqCount(max * 1000))

    for (u0 in 1..max) {

    }
}


val max = 100_000
//val max = 100_000_000

fun piSeqCount(n: Int): List<Int> {
    var u0 = n
    var k = 0
    val map = mutableListOf<Int>()
    var n = 0
    while (u0 != 1) {
        n++
        k += if (u0.toBigInteger().isProbablePrime(5)) 0 else 1
        u0 = pi2(u0)
        if (n >= 2) {
            map.add(k)
        }
    }
    map.add(k + 1)
    return map
}

var hashMapPi = hashMapOf<Int, Int>()

fun pi2(n: Int): Int {
    val value = primes().takeWhile { it <= n }.count()
    hashMapPi.put(n, value)
    return value
}