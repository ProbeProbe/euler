package euler

import kotlin.math.log2

fun main(args: Array<String>) {
    println(Long.MAX_VALUE)
    val strongRepunits = hashSetOf(1L)
    val max: Long;
//    max = 50
//    max = 1000
    max = 1_000_000_000_000L
    val repunits = Array(log2(max.toDouble()).toInt() + 1) { hashSetOf<Long>() }

    val max100 = Math.max(max / 1000, 1)

    for (base in 2..max) {
        if (base % max100 == 0L) {
            println("${base / max100}%")
            println(strongRepunits.size)
        }
        var prod = base + 1L//base*base+
        if (base % 100 == 0L)
            repunits.forEach { it.removeIf { it < prod } }
        var n = 2
        while (prod < max) {
//            if (repunits.withIndex().any { it.index>n && it.value.contains(prod) }) {
            if (repunits.filterIndexed { index, _ -> index > n }.any { it.contains(prod) }) {
                strongRepunits.add(prod)
//                repunits.remove(prod)
            } else if (n > 2) {
                repunits[n].add(prod)
            }
            prod *= base
            prod++
            n++
        }
    }
    println(strongRepunits.sorted())
    println(strongRepunits.sum())
}
//1000000000
//32962
//10849978873789
//10849978873789