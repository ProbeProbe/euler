package euler

import util.fibonacci

fun main(args: Array<String>) {
    val max = 100_000_000_000_000_000L

    val fib = fibonacci().takeWhile { it < max }.toList()
//    println(euler.zeck(100,fib))
    var sum = 0L
    for (i in 1 until max) {
        sum += zeck(i, fib)
    }
    println(sum)
    //falsch 92359637
}

fun zeck(n: Long, fib: List<Int>): Long {
    var m = n
    var c = 0L
    for (k in fib.size - 1 downTo 0) {
        val f = fib[k];
        if (f <= m) {
            m -= f
            c++
            if (m == 0L) {
                break
            }
        }
    }
    return c
}