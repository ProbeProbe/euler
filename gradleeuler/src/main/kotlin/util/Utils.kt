package util

fun modfac(n: Int, p: Int): Long {
    if (n >= p)
        return 0
    var result = 1L
    for (i in 2..n) {
        result = (result * i) % p
    }
    return result
}

fun <T> listToCountMap(list: List<T>): Map<T, Int> {
    return list.groupingBy { it }.eachCount()
}