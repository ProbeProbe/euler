package util

import java.math.BigInteger

fun primes(): Sequence<Int> {
    return generateSequence(BigInteger.valueOf(2)) { it.nextProbablePrime() }.map { it.intValueExact() }
}

fun fibonacci(): Sequence<Int> {
    return generateSequence(Pair(0, 1)) { Pair(it.second, it.first + it.second) }.map { it.first }
}