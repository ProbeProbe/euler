package euler;

import java.util.Arrays;

public class M113_114 {

    public static void main(String[] args) {
        euler114();
    }

    private static void euler114() {
        int maxLength = 50 + 5;
        int minD = 3;
        long[][] possibilities = new long[maxLength][maxLength];
        for (int len = 0; len < maxLength; len++) {
            for (int maxD = 0; maxD < maxLength; maxD++) {
                if (maxD < minD || maxD >= len - 2 || len < minD) {
                    possibilities[len][maxD] = 0;
                    continue;
                }
                //Here calculation
            }
        }
        //7x7 => 17
    }

    public static void euler113() {
        int[] last = new int[10];
        Arrays.fill(last, 1);
        for (int i = 0; i < 10; i++) {
            int count = 0;
            int[] newCount = new int[10];
            int sum = 0;
            for (int j = 0; j < 10; j++) {
                newCount[j] = 2 * sum + last[j];
                sum += last[j];
            }
            System.out.println(i + ": " + sum);
            last = newCount;
        }
    }
}
