import datetime

import gmpy2
import time

def primes(n=2):
    while True:
        yield n
        n = gmpy2.next_prime(n)

min=10**9
max=10**10-1

t1=datetime.datetime.now()

primeList=[]
for n in primes(min):
    if n>max:
        break
    if n>min:
        primeList.append(n)

t2=datetime.datetime.now()

diff=t2-t1
print("time",diff.seconds)
print(primeList[0])
print(primeList[-1])