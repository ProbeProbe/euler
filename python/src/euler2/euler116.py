
def poss(n,k):
    table = [1]+[0] * (k-1)
    table[-1]=1
    for i in range(k,n):
        # print(i,table,sum(table)-1)
        newtable=[0]*k
        newtable[0]=table[-1]
        for j in range(k-1):
            newtable[j+1]=table[j]
        newtable[k-1]+=table[-1]
        table=newtable
    # print(n, table,sum(table)-1)
    return sum(table)-1

def euler116(n):
    return poss(n,2)+poss(n,3)+poss(n,4)

print(euler116(5))
print(euler116(50))