import os
import pickle
from itertools import combinations
import itertools

words=open("p098_words.txt","r").read().replace("\"","").split(",")


def getChars(word):
    chars=[0]*255
    for c in word:
        chars[ord(c)]+=1
    return tuple(chars)
    # chars=dict()
    # for c in word:
    #     chars[c]=chars.get(c,0)+1
    # return chars

def getPalindromes(words):
    palin=dict()
    for w in words:
        chars=getChars(w)
        palin.setdefault(chars, []).append(w)
    return palin

palindromeDict=getPalindromes(words)
# print(getPalindromes(words))
palindromes=[]
for a,words in palindromeDict.items():
    # print(words)
    if len(words)>1:
        print(words)
        for comb in list(combinations(words,2)):
            palindromes.append(comb)


numbers=[]
# palindromes=[("CARE","RACE")]

length=max(map(len,map(lambda x: x[0],palindromes)))+1

print(length)

if os.path.isfile("squares.txt"):
    file = open('squares.txt', 'rb')
    squares = pickle.load(file)
    print("Squares loaded")
else:
    maxNum=10**length
    add=1
    c=0
    squares=[]
    while True:
        c+=add
        add+=2
        squares.append(c)
        if c>maxNum:
            break

    file = open('squares.txt', 'wb')
    pickle.dump(squares, file)
    file.close()
    print("Squares generated")

squares=set(squares)

for w1,w2 in palindromes:
    print(w1,w2)
    chars=list(set(w1))
    digits=combinations(range(0,10),len(chars))

    for d1 in digits:
        for a in itertools.permutations(d1):
            mapping=dict()
            for index,c in enumerate(chars):
                mapping[c]=a[index]
            if mapping[w1[0]]==0 or\
                mapping[w2[0]]==0:
                continue
            num1=0
            num2=0
            for c in w1:
                num1=10*num1+mapping[c]
            for c in w2:
                num2=10*num2+mapping[c]

            # print(mapping)
            # print(w1, num1)
            # print(w2, num2)
            if num1 in squares and\
                num2 in squares:
                numbers.append(num1)
                numbers.append(num2)
                # print(mapping)
                # print("Found")
                print(w1,num1)
                print(w2,num2)
                # exit(0)

print(numbers)
print(max(numbers))