def backpandigital(n):
    zc = [False] * 10
    for i in range(9):
        z = n % 10
        if z == 0:
            return False
        elif zc[z - 1]:
            return False
        else:
            zc[z - 1] = True
        n //= 10
    return True

def frontpandigital(n):
    if n<(10**12):
        return False
    digits=[]
    while n>0:
        digits.append(n%10)
        n//=10
    zc = [False] * 10
    for i in range(1,10):
        z = digits[-i]
        if z == 0:
            return False
        elif zc[z - 1]:
            return False
        else:
            zc[z - 1] = True
    return True


# 839725641

a = 1
b = 1

c=3
# for c in range(3, 542):
while True:
    a, b = (b, a + b)
    fib = b
    bp=backpandigital(fib)
    # print(c, bp, fp, fib)
    if bp:
        print(c,"back",fib)
        fp=frontpandigital(fib)
        if fp:
            print(c,"is also front")
            break
    c+=1