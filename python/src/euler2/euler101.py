import numpy


def poly(p, x):
    power = 0
    sum = 0
    while p != []:
        sum += p[-1] * pow(x, power)
        power += 1
        p = p[:-1]
    return sum


p=[1,-1,1,-1,1,-1,1,-1,1,-1,1]
# p = [1, 0, 0, 0]
max = 20
points = list(range(1, max + 5))
values = list(map(lambda x: poly(p, x), points))

sum=0
for r in range(max):
    fitted = list(numpy.polyfit(points[:r + 1], values[:r + 1], r))
    print(r, fitted)
    found=False
    for i in range(1,max):
        v = round(poly(fitted,i),3)
        if abs(v-values[i-1])>=1:
            found=True
            sum+=round(v)
            print("  at",i,"got",v,"expected",values[i-1])
            print("  sum",sum)
            break
    if not found:
        break
