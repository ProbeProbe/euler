import math

def divSum(n):
    result = 0
    for i in range(2, (int)(math.sqrt(n)) + 1):
        if (n % i == 0):
            if (i == (n / i)):
                result = result + i
            else:
                result = result + (i + n // i)
    return result+1

def buildChain(divSums, chain, max):
    number=chain[-1]
    next=divSums[number]
    if next>max:
        return [(c,-1) for c in chain]
    elif next in chain:
        index=chain.index(next)
        l=len(chain)-index
        return [(c,-1 if ind<index else l) for ind,c in enumerate(chain)]
        # return chain,len(chain)
    else:
        return buildChain(divSums,chain+[next],max)

print(divSum(220))
print(divSum(284))

maxNum=1000000
divSums=[0]*(maxNum + 1)
chainlength=[0]*(maxNum + 1)
for i in range(maxNum + 1):
    divSums[i]=divSum(i)

print("built")

for i in range(maxNum + 1):
    if chainlength[i] != 0:
        continue
    chain=buildChain(divSums, [i], maxNum)
    for e,l in chain:
        chainlength[e]=l

for i in range(maxNum + 1):
    print(i,chainlength[i])

minElement=max(chainlength)
minInd=chainlength.index(minElement)
print(minInd,minElement)


print("finished")
#203034 102