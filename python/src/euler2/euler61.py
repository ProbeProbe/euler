def triangle(n):
    return n*(n+1)/2

def square(n):
    return n*n

def pent(n):
    return n*(3*n-1)/2

def hex(n):
    return n*(2*n-1)

def hept(n):
    return n*(5*n-3)/2

def oct(n):
    return n*(3*n-2)

def generalNext(last,diff,order):
    return (diff+order,last+diff)

def findBacktrack(numbers,index,indices,digits,callstack):
    for n in numbers[index]:
        if digits == n//100 or digits == -1:
            if indices==[]:
                if n%100==callstack[0][1]//100:
                    newstack=callstack+[(index+3,n)]
                    sum=0
                    for ind,z in newstack:
                        sum+=z
                    print(newstack)
                    print("Summe: ",sum)
                continue
            for i in range(len(indices)):
                indices2=indices[:i]+indices[i+1:]
                findBacktrack(numbers, indices[i], indices2, n % 100,callstack+[(index+3,n)])


generators=[("3",triangle),
            ("4",square),
            ("5",pent),
            ("6",hex),
            ("7",hept),
            ("8",oct)]
min=1000
max=9999

numbers=[]

for name,g in generators:
    n=0
    numberlist=[]
    while True:
        n+=1
        num=g(n)
        if num>max:
            break
        elif num>=min:
            if num%100>=10:
                numberlist.append(num)
    numbers.append(numberlist)
    print(name,len(numberlist))

count=len(numbers)-1
indices=list(range(count))

solutions=[]
findBacktrack(numbers,count, indices,-1,[])
