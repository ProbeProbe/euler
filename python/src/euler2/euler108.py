from functools import reduce

def prime_factors(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return factors

def countingList(l):
    last=None
    count=0
    newlist=[]
    for e in l:
        if e!=last:
            if last!=None:
                newlist.append((last,count))
            count=0
            last=e
        count+=1
    if count>0:
        newlist.append((last,count))
    return newlist

def dividers(n):
    l=countingList(prime_factors(n))
    count=reduce(lambda x, y: x*y, map(lambda x: 2*x[1]+1,l))
    count=(count+1)/2
    return count

print(countingList(prime_factors(14)))
print(countingList(prime_factors(15)))
print(countingList(prime_factors(644)))
print(countingList(prime_factors(645)))
print(countingList(prime_factors(646)))
# print(dividers(2387503425234523189654))

print(countingList(prime_factors(4)))
print(dividers(4))

for i in range(4,1000000):
    d=dividers(i)
    if d>1000:
        print(i,d)
        break