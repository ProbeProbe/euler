import random

board=[
    "GO","A1","CC1","A2","T1","R1","B1","CH1","B2","B3",
    "JAIL","C1","U1","C2","C3","R2","D1","CC2","D2","D3",
    "FP","E1","CH2","E2","E3","R3","F1","F2","U2","F3",
    "G2J","G1","G2","CC3","G3","R4","CH3","H1","T2","H2"
]

community=["GO","JAIL"]+[""]*14
chance=["GO","JAIL","C1","E3","H2","R1","+R","+R","+U","-3"]+[""]*6

def diceRoll():
    a=random.randint(1,6)
    b=random.randint(1,6)
    return a+b,a==b

def game(moves,community,chance):
    random.shuffle(community)
    random.shuffle(chance)
    count=0
    # field=random.randint(0,len(board)-1)
    field=0
    fieldchance=[0]*(len(board))
    for i in range(moves):
        s,b=diceRoll()
        if b:
            count+=1
        if count==3:
            count=0
            field=board.index("JAIL")
        else:
            field+=s
            if field>=len(board):
                field-=len(board)
            if board[field]=="G2J":
                field=board.index("JAIL")
            elif board[field].startswith("CH"):
                next=chance[0]
                chance=chance[1:]+[next]
                if next!="":
                    if next[0]=="-":
                        field+=int(next)
                        if field<0:
                            field+=len(board)
                    elif next[0]=="+":
                        while not board[field].startswith(next[1:]):
                            field+=1
                            if field>=len(board):
                                field-=len(board)
                    else:
                        field=board.index(next)
            elif board[field].startswith("CC"):
                next=community[0]
                community=community[1:]+[next]
                if next!="":
                    field=board.index(next)
        fieldchance[field]+=1
    return fieldchance

probs=[0]*len(board)
moves=1000000
games=100
#100
for i in range(games):
    print("Game",i,"/",games)
    prob=game(1000000,community,chance)
    for j in range(len(board)):
        probs[j]+=prob[j]

for index,prob in sorted(enumerate(probs),key=lambda x: x[1]):
    print(index,prob)