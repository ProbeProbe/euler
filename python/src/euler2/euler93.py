import itertools

add = lambda x, y: x + y
sub = lambda x, y: x - y
mul = lambda x, y: x * y
div = lambda x, y: 10000 if y == 0 else (x / y)


def eval(a, b, c, d, f1, f2, f3, n1, n2, n3):
    return [(f1(f2(f3(a, b), c), d), f"{n1}({n2}({n3}({a}, {b}), {c}), {d})"),
            (f1(f2(a, f3(b, c)), d), f"{n1}({n2}({a}, {n3}({b}, {c})), {d})"),
            (f1(f2(a, b), f3(c, d)), f"{n1}({n2}({a}, {b}), {n3}({c}, {d}))"),
            (f1(a, f2(f3(b, c), d)), f"{n1}({a}, {n2}({n3}({b}, {c}), {d}))"),
            (f1(a, f2(b, f3(c, d))), f"{n1}({a}, {n2}({b}, {n3}({c}, {d})))")]


def poss(a, b, c, d):
    numbers = []
    for x1, x2, x3, x4 in itertools.permutations([a, b, c, d]):
        for fn1, fn2, fn3 in itertools.product([(add,"add"), (sub,"sub"), (mul,"mul"), (div,"div")], repeat=3):
            f1,n1=fn1
            f2,n2=fn2
            f3,n3=fn3
            newnumbers = eval(x1, x2, x3, x4, f1, f2, f3,n1,n2,n3)
            numbers.extend(newnumbers)
    numbers = set(filter(lambda x: int(x[0])==x[0] and x[0] > 0, numbers))
    return numbers


def chainLength(numsT):
    last = 0
    count = 0
    nums=list(set(map(lambda x: x[0],numsT)))
    print("   ",sorted(nums))
    for n in sorted(nums):
        if n - last > 1:
            return count
        count += 1
        last = n
    return count


def chainLength2(nums):
    last = -2
    count = 0
    chains = []
    for n in nums:
        if n - last > 1:
            chains.append(count)
            count = 0
        count += 1
        last = n
    return max(chains)


# num=poss(1,2,3,4)
# print(num)
# print(chainLength(num))
# print(len(num))

chains = dict()
for a, b, c, d in itertools.combinations(range(0, 10), 4):
    numbers = poss(a, b, c, d)
    chainLen = chainLength(numbers)
    print(a, b, c, d, "=>", chainLen)
    # print("    ", sorted(numbers))
    chains[(a, b, c, d)] = (chainLen,numbers)

maxkey = max(chains, key=lambda x: chains[x][0])
chain=chains[maxkey]
numbers=sorted(chain[1],key=lambda x: x[0])
print(maxkey, chain)
print(numbers)
d=dict()
for n,s in numbers:
    d.setdefault(n,[]).append(s)

for k,sl in d.items():
    print(k,sl)
