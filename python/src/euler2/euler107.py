
class Node:

    def __init__(self ,id):
        self.edges =[]
        self.id =id

    def addedge(self ,n ,w):
        self.edges.append((n ,w))


def getMST(node,append,visited):
    pass


def generateGraph(matrix):
    nodes = []
    for i in range(len(matrix)):
        nodes.append(Node(i))

    for i, row in enumerate(matrix):
        print(row)
        for j, k in enumerate(row):
            if k == 0:
                continue
            nodes[i].addedge(nodes[j], k)

    return nodes


matrix=list(map(lambda x:
                # x.split(",")
            list(map(int,x.split(",")))
           # list(map(lambda y: 0 if y=="-" else int(y),x.split(",")))
            ,
           open("p107_network.txt","r").read().replace("-","0").split("\n")[:-1]))
# matrix=list(map(lambda x: list(map(lambda y: 999999 if y==0 else y,x)),matrix))
# print(matrix)


from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree


def test(matrix):
    nodes=generateGraph(matrix)
    graph=csr_matrix(matrix)
    Tcsr = minimum_spanning_tree(graph)
    mst=Tcsr.toarray().astype(int)
    print()
    for row in mst:
        print(row)
    oldSum=sum(map(sum,matrix))//2
    newSum=sum(map(sum,mst))
    print("old Sum",oldSum)
    print("new Sum",newSum)
    print("diff",oldSum-newSum)

test([[0,16,12,21,0,0,0],
     [16,0,0,17,20,0,0],
     [12,0,0,28,0,31,0],
     [21,17,28,0,18,19,23],
     [0,20,0,18,0,0,11],
     [0,0,31,19,0,0,27],
     [0,0,0,23,11,27,0]])
test(matrix)