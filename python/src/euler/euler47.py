def prime_factors(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return factors


print(prime_factors(14))
print(prime_factors(15))
print(prime_factors(644))
print(prime_factors(645))
print(prime_factors(646))
