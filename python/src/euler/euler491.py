import itertools
import numpy as np


def alternativeSum(x):
    sum = 0
    mult = 1
    for i in x:
        sum += mult * i
        mult = -mult
    return sum


part = list(itertools.permutations(range(0, 10)))
print(part)
sum = list(map(alternativeSum, part))
print(sum)
count=0
length=len(part)
for i in range(length):
    if i%100==0:
        print(i,"/",length)
    if part[i][0] == 0:
        continue
    for j in range(length):
        if sum[i]+sum[j] == 0:
            count += 1
print("Count: ", count)